#!/bin/bash

source config.sh

for repo in "${REPOS[@]}"; do
  echo -e "[+] Attempting to Clone $repo"
  if [ -d "$repo" ]; then
     continue;
  fi

  git clone git@gitlab.com:$GROUP/$repo.git ./$repo
done
