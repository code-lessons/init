#!/bin/bash

export GROUP='code-lessons'

export REPOS=(
  setup
  #dart
  frontend
  frontend/practice
  javascript/basic
  javascript/private
  react/app
  react/mashup
  react/planner
  #react-old/app
  #react-old/samples
)

